% Created 2021-06-29 Tue 12:58
% Intended LaTeX compiler: pdflatex
\documentclass[11pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\linespread{1.5}
\usepackage[margin=1in,footskip=0.25in]{geometry}
\usepackage{setspace}
\usepackage[font=small,labelfont=bf,tableposition=bottom]{caption}
\captionsetup[figure]{font={stretch=1.1}}
\DeclareCaptionLabelFormat{supp_cap}{#1 S#2}
\captionsetup[suppfigure]{font={stretch=1.1}, labelformat=supp_cap}
\captionsetup[supptable]{font={stretch=1.1}, labelformat=supp_cap}
\newcounter{suppdata_global}
\DeclareCaptionLabelFormat{suppdata}{#1 \arabic{suppdata_global}.#2}
\captionsetup[suppdata]{font={stretch=1.1}, labelformat=suppdata}
\usepackage{wrapfig}
\setcounter{secnumdepth}{5}
\usepackage{breakcites}
\usepackage{subcaption}
\usepackage{adjustbox}
\usepackage{pdfpages}
\usepackage[shortlabels]{enumitem}
\usepackage{tikz}
\usetikzlibrary{trees}
\usepackage{forest}
\input{/home/kantundpeterpan/my_stuff_on_server/thesis/master/forest_setup.tex}
\usepackage{newfloat}
\DeclareFloatingEnvironment[name={Supplementary Figure}, listname={List of Supplementary Figures}]{suppfigure}
\DeclareFloatingEnvironment[name={Supplementary Table}, listname={List of Supplementary Figures}]{supptable}
\DeclareFloatingEnvironment[name={Supplementary Data}, listname={List of Supplementary Data}]{suppdata}
\usepackage{booktabs}
\usepackage{multirow}
\setcounter{secnumdepth}{6}
\author{haina}
\date{\today}
\title{}
\hypersetup{
 pdfauthor={haina},
 pdftitle={},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.3 (Org mode 9.4.4)}, 
 pdflang={English}}
\begin{document}


\section{Objectives}
\label{7:pg_label:preintro}
The chemical composition of the PG layer has been reported for a wide range of bacterial species \cite{Quintela1995}. However, the dynamics of its expansion during growth and maturation are less well understood. A range of models for the insertion of new subunits into the existing PG layer have been proposed (for a commented selection of models see Fig. \ref{pg_label:pre_intro:fig:models}).

\begin{figure}[htbp]
\centering
\includegraphics[width=0.9\linewidth]{figures/3_models.png}
\caption{\label{pg_label:pre_intro:fig:models}Models for the insertion of new PG units into the existing PG layer, adapted from \cite{Holtje1998} (\textbf{A}) Neo-synthesized (new) material is not attached to pre-existing (old) material, crosslinks should exclusively link newly synthesized material (\textbf{B}) \textbf{One strand at a time}, all crosslinks are expected to contain old and new material (\textbf{C}) \textbf{Three-for-one}, three strands are polymerized simultaneously and replace a docking strand which is removed; crosslinks are expected to occur both between new and preexisting subunits and between new subunits}
\end{figure}

Experiments investigating the insertion of new subunits into PG and the fate of PG during growth have historically been based on pulse labeling of cultures of \emph{E. coli} with [\textsuperscript{3}H]- or [\textsuperscript{14}C]-labeled DAP \cite{Wientjes1985,Burman1984a,Glauner1990}.
The usage of DAP as tracer molecule is challenging. While DAP residues are exclusively found in PG, a large proportion of DAP is converted into \textsc{l}-lysine. Efficient labeling of PG with DAP necessitates an adjustment of the metabolic fluxes in \emph{E. coli}, namely (i) deletion of \emph{lysA} (coding for the diaminopimelate decarboxylase) in order to prevent conversion of DAP to \textsc{l}-lysine (ii) inactivation of \emph{dapF} (coding for the diaminopimelate epimerase) to abolish endogenous biosynthesis of DAP, and (iii) adding \textsc{l}-lysine and DAP to the culture medium, due the auxotrophy for these amino acids induced by the aforementioned gene deletions.
\emph{E. coli} W7 (\emph{dapF}, \emph{lysA}), which was used in various studies \cite{Goodell1985a,Burman1983,Burman1984a,Glauner1990} harbors a large intracellular pool of DAP when grown in the media supplemented with DAP (usually 2 µg/ml). This DAP pool induces a lag between the addition of radioactive DAP and appearance of the label in the metabolites of interest, such as intracellular peptidoglycan precursors and muropeptides of the PG layer. The duration of the lag depends on the amount of DAP present in the growth medium \cite{Wientjes1985}.

Becaus of this limitation, we aimed at developing a new labeling method using heavy stable isotopes of carbon [\textsuperscript{13}C] and nitrogen [\textsuperscript{15}N] and mass spectrometry that would not to require any specific genetic background to enable fast labeling kinetics. We compared our method to radioactive DAP-based techniques in terms of lag time prior to the detection of newly synthesized material and their ability to distinguish muropeptides issued from recycling and from \emph{de novo} synthesis.
Our method was also used to characterize differences between the modes of PG cross-linking mediated by PBPs and LDTs. The results are presented in the form of a an article in preparation for submission the the journal eLife.

Our MS-based approach necessitated (i) the calculation of theoretical mass spectra of muropeptides containing combinations of labeled and unlabeled residues starting from their isotopic composition and chemical formula and (ii) the development of custom data preprocessing and analysis tools, comprising batch analysis of data files, automated peak annotation in mass spectra and comparison of experimental data to theoretical fragmentation patterns of muropeptides.
Calculation of theoretical mass spectra was performed using IsoTool \cite{heiner_atze_2021_5031787}, which provides a graphical user interface for the IsoSpec algorithm for isotopic cluster calculations \cite{Lacki2017} (available at \url{https://github.com/kantundpeterpan/IsoTool}).
Tools for MS data analysis have been grouped into MassElTOF package (\url{https://gitlab.com/kantundpeterpan/masseltof}), which contains example applications in its documentation.

\section{Publication III}
\label{7:pg_label:pubIII}
 \includegraphics[width=0.95\textwidth, trim=62 40 62 60, clip, page=1]{/home/kantundpeterpan/my_stuff_on_server/thesis/7_pg_labeling/master/export/master.pdf} 
\includepdf[pages=2-,pagecommand={}, rotateoversize]{/home/kantundpeterpan/my_stuff_on_server/thesis/7_pg_labeling/master/export/master.pdf}


\section*{\ldots{}}
\label{sec:org1688761}
\clearpage
\bibliographystyle{apalike}
\bibliography{../../bibliography/library}
\end{document}
