% Created 2021-07-12 Mon 16:40
% Intended LaTeX compiler: pdflatex
\documentclass[11pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\linespread{1.5}
\usepackage[margin=1in,footskip=0.25in]{geometry}
\usepackage{setspace}
\usepackage[font=small,labelfont=bf,tableposition=bottom]{caption}
\captionsetup[figure]{font={stretch=1.1}}
\DeclareCaptionLabelFormat{supp_cap}{#1 S#2}
\captionsetup[suppfigure]{font={stretch=1.1}, labelformat=supp_cap}
\captionsetup[supptable]{font={stretch=1.1}, labelformat=supp_cap}
\newcounter{suppdata_global}
\DeclareCaptionLabelFormat{suppdata}{#1 \arabic{suppdata_global}.#2}
\captionsetup[suppdata]{font={stretch=1.1}, labelformat=suppdata}
\usepackage{wrapfig}
\setcounter{secnumdepth}{5}
\usepackage{breakcites}
\usepackage{subcaption}
\usepackage{adjustbox}
\usepackage{pdfpages}
\usepackage[shortlabels]{enumitem}
\usepackage{tikz}
\usetikzlibrary{trees}
\usepackage{forest}
\input{/home/kantundpeterpan/my_stuff_on_server/thesis/master/forest_setup.tex}
\usepackage{newfloat}
\DeclareFloatingEnvironment[name={Supplementary Figure}, listname={List of Supplementary Figures}]{suppfigure}
\DeclareFloatingEnvironment[name={Supplementary Table}, listname={List of Supplementary Figures}]{supptable}
\DeclareFloatingEnvironment[name={Supplementary Data}, listname={}]{suppdata}
\usepackage{booktabs}
\usepackage{multirow}
\setcounter{secnumdepth}{6}
\author{haina}
\date{\today}
\title{}
\hypersetup{
 pdfauthor={haina},
 pdftitle={},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.3 (Org mode 9.4.4)}, 
 pdflang={English}}
\begin{document}

\graphicspath{{./figures/}}

\section*{Abstract}
\label{7:pg_label:abstract}
\begin{abstract}
\linespread{1}\selectfont
Peptidoglycan, the major component of bacterial cell walls, is a giant
net-like macromolecule made of glycan strands cross-linked by short
peptides. Since peptidoglycan is essential to sustain the osmotic
pressure of the cytoplasm and is only found in bacteria the enzymes
involved in its synthesis are attractive targets for drug development.
This is the case for D,D-transpeptidases belonging to the
penicillin-binding protein (PBP) family that catalyze the last
cross-linking step of peptidoglycan polymerization and are inactivated
by drugs of the $\beta$-lactam family such as ampicillin. These enzymes form
4→3 cross-links connecting the 4\textsuperscript{th} and
3\textsuperscript{rd} positions of acyl donor and acceptor stem peptides
located in adjacent glycan chains. In \emph{Escherichia coli}, bypass of
PBPs through formation of 3→3 cross-links by L,D-transpeptidase YcbB
results in ampicillin resistance. Here, the mode of insertion of
neo-synthesized peptidoglycan subunits into the expanding peptidoglycan
macromolecule was investigated based on labeling with heavy isotopes and
mass spectrometry. This new approach offered several advantages over
previously used labeling with a radioactive precursor since it enabled
analysis of peptidoglycan synthesis and recycling in a single experiment
and did not require metabolic engineering for labeling optimization. In
the absence of ampicillin, PBPs and YcbB participated in similar modes
peptidoglycan polymerization mainly involving single-strand insertion of
glycan chains into the expanding side walls. The absence of any
transpeptidase-specific signature suggests that the mode of insertion of
glycan chains might be determined by other components of the
peptidoglycan polymerization complexes. In the presence of ampicillin,
YcbB mediated insertion of multiple strands that were exclusively
cross-linked to tripeptide-containing acceptors present in the
preexisting peptidoglycan. We propose that this unique mode of insertion
depends upon accumulation of linear glycan chains due to PBP inhibition
by ampicillin and formation of tripeptide acceptors due to cleavage of
pre-existing 3→3 cross-links by a $\beta$-lactam-insensitive endopeptidase.
\end{abstract}

\section{Introduction}
\label{7:pg_label:intro}
\setcounter{suppfigure}{0}
The peptidoglycan is an essential component of the bacterial cell wall
that provides a mechanical barrier against the turgor pressure of the
cytoplasm, thereby preventing cells from bursting and lysing \cite{Vollmer2008}. The
peptidoglycan also determines the shape of bacterial cells and is
intimately integrated into the cell division process because the barrier
to the osmotic pressure needs to be maintained during the entire cell
cycle. These functions depend upon the net-like structure of the
peptidoglycan macromolecule (\emph{ca}. 4.2 x 10\textsuperscript{9} Da in \emph{Escherichia
coli}) consisting of glycan strands cross-linked by short peptides (Fig. \ref{pg_label:intro:fig:pg_struc}A) \cite{Mengin-Lecreulx1982}. It is assembled from \emph{ca.} 4.7 x 10\textsuperscript{6} disaccharide-peptide
subunits consisting of \(\beta\)-1\(\rightarrow\)4 linked \emph{N}-acetylglucosamine (GlcNAc) and
\emph{N}-acetyl muramic acid (MurNAc) and a stem peptide linked to the
\textsc{d}-lactoyl group of MurNAc (Fig. \ref{pg_label:intro:fig:pg_struc}B) \cite{Mengin-Lecreulx1982}.
Polymerization of the subunits is mediated by glycosyltransferases for the elongation of glycan chains
and by transpeptidases for cross-linking stem peptides carried by adjacent glycan chains.

Peptidoglycan biosynthesis is initiated in the cytoplasm by the
production of UDP-MurNAc from UDP-GlcNAc followed by the sequential
addition of L-Ala, \textsc{d}-Glu, \emph{meso}DAP, and the \textsc{d}-Ala-\textsc{d}-Ala dipeptide to
form UDP-MurNAc-pentapeptide (Supplementary Fig. S\ref{pg_label:intro:sfig:pg_metabolism}) \cite{Barreteau2008}. The following
steps involve precursors inserted into the inner leaflet of the
cytoplasmic membrane. These steps are initiated by the transfer of the
phospho-MurNAc-pentapeptide moiety of UDP-MurNAc-pentapeptide to the
C\textsubscript{55} lipid transporter (undecaprenyl-phosphate) followed by the
addition of the GlcNAc moiety of UDP-GlcNAc generating Lipid I and Lipid
II, respectively. The complete disaccharide-pentapeptide subunit linked
to the lipid carrier is then translocated to the outer surface of the
cytoplasmic membrane by the flippase MurJ and/or FtsW and polymerized.

Following the polymerization of glycan chains by glycosyltransferases,
the last cross-linking step of peptidoglycan polymerization is catalyzed
by two types of structurally unrelated transpeptidases \cite{Biarrotte-Sorin2006}, the
\textsc{d},\textsc{d}-transpeptidases \cite{Zapun2008}, which belong to the penicillin-binding protein
(PBP) family, and the L,\textsc{d}-transpeptidases (LDTs) \cite{Mainardi2008}. PBPs (Fig. \ref{pg_label:intro:fig:pg_struc}C) and
LDTs (Fig. \ref{pg_label:intro:fig:pg_struc}D) harbor different catalytic nucleophiles (Ser \emph{versus}
Ala), use different acyl donor substrates (pentapeptide \emph{versus}
tetrapeptide, resulting in the formation of 4\(\rightarrow\)3 \emph{versus} 3\(\rightarrow\)3
cross-links), and are inhibited by different classes of \(\beta\)-lactams
(potentially all \(\beta\)-lactams \emph{versus} members of the carbapenem class),
respectively \cite{Mainardi2005,Hugonnet2016}. In wild type \emph{E. coli}, LDTs are fully dispensable
for growth, at least in laboratory conditions \cite{Magnet2008}. Accordingly, these
enzymes have a minor contribution to peptidoglycan cross-linking, as 3\(\rightarrow\)3
cross-links account for 11\% and 16\% of the cross-links present in the
peptidoglycan extracted from bacteria in the exponential and stationary
phases of growth, respectively \cite{Glauner1988,Vollmer2008}. However, selection of mutants
resistant to ceftriaxone, a \(\beta\)-lactam of the cephalosporin class, results
in a full bypass of PBPs by the YcbB \textsc{l},\textsc{d}-transpeptidase \cite{Hugonnet2016}. The bypass
requires overproduction of the (p)ppGpp alarmone and of YcbB \cite{Hugonnet2016}. The
peptidoglycan of the mutant grown in the presence of ceftriaxone
exclusively contains cross-links of the 3\(\rightarrow\)3 type due to full inhibition
of the \textsc{d},\textsc{d}-transpeptidase activity of PBPs \cite{Hugonnet2016}.

\textsc{d},\textsc{d}-carboxypeptidase and \textsc{l},\textsc{d}-carboxypeptidase activities sequentially
remove \textsc{d}-Ala residues at the 5\textsuperscript{th} and 4\textsuperscript{th} positions of stem
peptides containing a free carboxyl end, which are present in the
acceptor position of dimers and in monomers. \textsc{d},\textsc{d}-carboxypeptidases
(DDCs), which belong to the PBP family, hydrolyze the
\textsc{d}-Ala\textsuperscript{4}-\textsc{d}-Ala\textsuperscript{5} amide bond of stem pentapeptides to generate stem
tetrapeptides in mature peptidoglycan \cite{Sauvage2008}. Since that reaction is
nearly total in \emph{E. coli}, stem pentapeptides are found in very low
abundance unless DDCs are inactivated by \(\beta\)-lactams. The
\textsc{l},\textsc{d}-carboxypeptidase activity of LDTs is responsible for hydrolysis of the
DAP\textsuperscript{3}-\textsc{d}-Ala\textsuperscript{4} amide bond, thus converting tetrapeptide stems into
tripeptide stems \cite{Magnet2008}. Since this reaction is incomplete, the
peptidoglycan contains combinations of tetrapeptide and tripeptide stems
in monomers and in the acyl acceptor position of dimers. The hydrolysis
of stem peptides catalyzed by DDCs and LDTs, which occurs before or
after the cross-linking reactions, leads to the polymorphism in the
acceptor position of dimers depicted in Fig. \ref{pg_label:intro:fig:pg_struc}C and \ref{pg_label:intro:fig:pg_struc}D.

By removing \textsc{d}-Ala\textsuperscript{5} from pentapeptide stems, DDCs eliminate the
essential pentapeptide donor of PBPs and generate the essential
tetrapeptide donor of LDTs \cite{Hugonnet2016,Mainardi2002}. By removing \textsc{d}-Ala\textsuperscript{4} from
tetrapeptide stems, LDCs eliminate their essential tetrapeptide donor
stems. Thus, the DDC and LDC activities of PBPs and LDTs regulate both
the extent of peptidoglycan cross-linking and the relative contributions
of \textsc{d},\textsc{d}-transpeptidases and \textsc{l},\textsc{d}-transpeptidases to this process at the
substrate level .

The interconversion between monomers and dimers involves not only PBPs
and LDTs in the biosynthetic direction but also hydrolytic enzymes,
referred to as endopeptidases, that cleave the cross-links.
Endopeptidases belonging to the PBP family are specific of 4\(\rightarrow\)3 cross-links whereas other endopeptidases cleave both 4\(\rightarrow\)3 and 3\(\rightarrow\)3 cross-links \cite{Singh2012,Voedts2021}. Among eight endopeptidases with partially
redundant functions, at least one enzyme is essential in the context of
the formation of 4\(\rightarrow\)3 cross-links whereas two endopeptidases, MepM and
MepK, are required in the context of 3\(\rightarrow\)3 cross-links \cite{Voedts2021}.

Morphogenesis of \emph{E. coli} cells depends upon controlled expansion of
the peptidoglycan and requires scaffolding proteins, homologues of
tubulin (MreB) and of actin (FtsZ) \cite{DenBlaauwen2008}. These proteins are involved in
the localization of peptidoglycan polymerases and in the coordination of
their activities with that of hydrolases \cite{Vollmer2001}. The cell cycle involves
two phases corresponding to the MreB-dependent elongation of the side
wall at a constant diameter followed by FtsZ-dependent formation of the
septum at mid cell. These processes are thought to involve two distinct
multienzyme complexes, the elongasome and the divisome, and specific
enzymes, in particular PBP2 and PBP3 (class B PBPs), that mediate
peptidoglycan cross-linking in the side wall and in the septum,
respectively \cite{Holtje1996}. Accordingly, specific inhibition of PBP2 or PBP3 by
the \(\beta\)-lactam mecillinam or aztreonam results in the growth of \emph{E. coli}
as spheres or as filaments, respectively.

The average chemical composition of the peptidoglycan is well known.
Most enzymes involved in the formation or cleavage of glyosidic and
amide bonds have been identified (at least one enzyme and generally
several enzymes for each bond kind). However, the spatial arrangement of
glycan strands is still a matter of debate. Most models propose a
regular arrangement of glycan strands parallel to the cell surface and
perpendicular to the long axis of the cell as depicted in Fig. \ref{pg_label:intro:fig:pg_struc}A \cite{Typas2011},
perhaps with an helical arrangement to account for the helical nature of
MreB filaments. Alternatively, it has been proposed that the glycan
strands protrude perpendicularly to the surface of the cytoplasmic
membrane \cite{Meroueh2006,Dmitriev2003}. The lack of direct experimental evidence originate
from the heterogeneity of the peptidoglycan macromolecule that prevents
the analysis of the polymer by radiocrystallography. NMR also failed to
determine the structure of the polymer due to its heterogeneity and its
important flexibility as measured by NMR relaxation for Gram-positive
and Gram-negative bacteria \cite{Kern2008}. The mode of insertion of new glycan
strands in the growing peptidoglycan is also not fully characterized.
Murein enlargement according to the ‘three-for-one' growth model
involves the insertion of three neosynthesized glycan strands to the
detriment of the hydrolysis of one preexisting strand, which acts as a
docking strand \cite{Vollmer2001}. Other models propose that glycan strands are
inserted either one or two at a time, the latter possibility being
supported by the dimeric nature of certain \textsc{d},\textsc{d}-transpeptidases \cite{Charpentier2002}.
These models postulate that expansion of the peptidoglycan layer
requires cleavage of cross-links by endopeptidases and obey to the
‘make-before-break' principle, that proposes that cross-links present in
the preexisting peptidoglycan are hydrolyzed by endopeptidases only if
the newly synthesized glycan strands are cross-linked and can sustain
the osmotic pressure of the cytoplasm.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{figures/pg_overview.png}
\caption[Structure and biosynthesis of \emph{E. coli} peptidoglycan]{\label{pg_label:intro:fig:pg_struc}\textbf{Structure and biosynthesis of \emph{E. coli} peptidoglycan} (\textbf{A}) Structure of peptidoglycan. The net-like macromolecule is made of glycan strands cross-linked by short peptides. The polymerization of peptidoglycan involves glycosyltransferases that catalyze the elongation of glycan strands and transpeptidases that form 4\(\rightarrow\)3 or 3\(\rightarrow\)3 cross-links. (\textbf{B}) Structure of the peptidoglycan subunit. The stem peptide is assembled in the cytoplasm as a pentapeptide, \textsc{l}-Ala\textsuperscript{1}-\textsc{d}-iGlu\textsuperscript{2}-DAP\textsuperscript{3}-\textsc{d}-Ala\textsuperscript{4}-\textsc{d}-Ala\textsuperscript{5}, in which \textsc{d}-\emph{iso}-glutamic acid (\textsc{d}-iGlu) and \emph{meso}-diaminopimelic acid (DAP) are connected by an amide bond between the \(\gamma\)-carboxyl of \textsc{d}-iGlu and the L stereo-center of DAP. (\textbf{C}) Formation of 4 \(\rightarrow\)3 cross-links. Active-site Ser transpeptidases belonging to the penicillin-binding protein (PBP) family catalyze the formation of 4\(\rightarrow\)3 cross-links connecting the carbonyl of \textsc{d}-Ala at the 4\textsuperscript{th} position of an acyl donor stem peptide to the side-chain amino group of DAP at the 3\textsuperscript{rd} position of an acyl acceptor stem peptide. A pentapeptide stem is essential in the donor substrate to form the acyl enzyme intermediate. The acyl acceptor potentially harbor a pentapeptide, a tetrapeptide, or a tripeptide stem leading to the formation of Tetra-Tri, Tetra-Tetra, or Tetra-Penta dimers, respectively. (\textbf{D}) Formation of 3\(\rightarrow\)3 cross-links. Active-site Cys transpeptidase belonging to the \textsc{l},\textsc{d}-transpeptidase (LDT) family catalyze the formation of 3\(\rightarrow\)3 cross-links connecting two DAP residues. LDTs are specific of tetrapeptide-containing donors.}
\end{figure}

At each generation, about half of the disaccharide-peptide units making
the peptidoglycan is released by the combined action of endopeptidases
and lytic glycosyltransferases \cite{Goodell1985a,Johnson2013}. The latter enzymes catalyze a
non-hydrolytic cleavage of the \(\beta\)-1\(\rightarrow\)4 MurNAc-GlcNAc glycosylic bond and
generate GlcNAc-anhydro-MurNac-peptide fragments via cyclisation at
positions 1 and 6 of MurNAc. These fragments are transported into the
cytoplasm by the AmpG permease and recycled according to the pathway
depicted in supplementary Fig. S\ref{pg_label:intro:sfig:pg_metabolism}.
The key features of the recycling pathway are that the \textsc{l}-Ala-\(\gamma\)-\textsc{d}-Glu-DAP tripeptide is directly recycled
whereas the glucosamine moiety of GlcNAc and MurNAc reenter into the
peptidoglycan biosynthesis pathway at the level of
glucosamine-1-phosphate. The relationships between peptidoglycan
synthesis and recycling are poorly understood. The ‘three-for-one'
growth model predicts that the docking strand is recycled, while the
“one-or-two-strand-at-a-time” models do not predict that recycling is a
consequence of peptidoglycan synthesis.

Pulse-labeling of peptidoglycan with radioactive DAP provides a means
for experimentally exploring these models since the ratio of
radioactivity in acceptor stems to that in donor stems varies as a
function of the model (acceptor-to-donor radioactive ratio or ADRR)
(Fig. S\ref{pg_label:intro:sfig:adrr_calcs}) \cite{DeJonge1989}. Alternatively, the decay of radioactive DAP from
labeled peptidoglycan was used to follow peptidoglycan turnover. In this
study, we show that the use of stable isotopes of carbon and nitrogen in
combination with mass spectrometry (MS) has several advantages over the
labeling techniques based on radioactive DAP because it enables
following peptidoglycan synthesis and recycling in a single experiment.
In addition, the MS-based technique is not limited to the fate of DAP
but covers all sugar and amino acid constituents of peptidoglycan. Using
this approach, we also report the mode of peptidoglycan cross-linking by
LDTs which has not been previously investigated.


\begin{suppfigure}
\centering
\includegraphics[width=.9\linewidth]{pg_metabolism.png}
\caption[Synthesis and recycling of peptidoglycan in \emph{E.
coli}]{\label{pg_label:intro:sfig:pg_metabolism}
\textbf{Synthesis and recycling of peptidoglycan in \emph{E.
coli}}. Key intermediates connecting synthesis (on the left) to
recycling (on the right) are figured in red. Metabolites common to
peptidoglycan metabolism and to other metabolic pathways are figured in
purple. Effective incorporation of DAP from the culture medium requires
inactivation of the genes encoding DapF and LysA generating lysine
auxotrophy. Peptidoglycan turnorver refers to the release of peptidoglycan fragments (<600 Da) into the
culture medium. PG fragments cross the outer membrane through porins.}
\end{suppfigure}

\begin{suppfigure}
\centering
\includegraphics[width=.9\linewidth]{adrr_models/adrr_calcs.png}
\caption[Acceptor donor radioactive ratio (ADRR) as a function of the number of glycan strands inserted at the same time in the peptidoglycan layer]
{\label{pg_label:intro:sfig:adrr_calcs}
(\textbf{A}) One strand at a time.
(\textbf{B}) Two strands at
a time.
(\textbf{C}) Three stands at a time (applies to the three for one
model).
(\textbf{D}) Calculation for various number of strands.
Preexisting and neo-synthesized strands are figures in red and in purple, respectively.
Donor and acceptor stems are represented by an arrow and by an L,
respectively. Uncross-liked peptide stems that protrude above and below
the plan are indicated by circles with a dot or an X, respectively.}
\end{suppfigure}

\section*{\ldots{}}
\label{sec:orgdc0a55a}
\clearpage
\bibliographystyle{apalike}
\bibliography{../../bibliography/library}
\end{document}
