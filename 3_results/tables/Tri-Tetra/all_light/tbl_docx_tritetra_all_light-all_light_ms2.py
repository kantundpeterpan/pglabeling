#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 29 18:24:42 2021

@author: kantundpeterpan
"""
import sys
import pandas as pd
import re
from subprocess import call
import numpy as np
sys.path.append('/media/kantundpeterpan/fast2/confinement/Python')

from MassAnalyzer.MS2Helper.FragDB.MS2DB import DimersMS2DB
from MassAnalyzer.MS2Helper.Tables import docx_table_unique, docx_table_ambig, frag_cell_docx, docx_table_parent
from docx import Document
from docx.enum.section import WD_ORIENT
from bokeh.io import export_svgs
import os

pwd = os.getcwd()

analysis_file = '/media/kantundpeterpan/fast2/confinement/Project Peptidoglycan/Exp 6 - M1 deriv M9 + Xtract light to heavy/analysis_scripts/exp6_tritetra_ms2_897.894.py'.replace(' ', '\\ ')

import IPython
ipython = IPython.get_ipython()
ipython.run_line_magic('run', '-i %s True' % analysis_file)

export_svgs(plot.p,
            filename = pwd + '/{parent}_{isotop}_{prec}_{sample}.svg'.format(
                        parent = parent.replace('-', ''),
                        isotop = '_'.join(hyb_labels),
                        prec = prec,
                        sample = sample),
            height = 600, width = 1200)

pd.set_option('display.max_colwidth',None)
pd.options.display.float_format = "{:,.3f}".format

color_map = {
        'C13N15':'heavy',
        'C12N14':'light'
    }

color_rgb = {
        'C13N15':'ff3333',
        'C12N14':'9933ff'
    }

basename = 'tbl_tritetra_all_light-all_light_897.891_ms2_xp6'
parent = 'Tri-Tetra'
parent_isotops = ['all light-all light']
parent_prec = 897.8908594
db = DimersMS2DB


doc = Document()

doc = docx_table_parent(parent, parent_isotops, parent_prec,
                        color_rgb, doc, db, z = 2)

doc.add_paragraph('')

if hasattr(y, 'found_unique'):
    df = pd.concat(y.found_unique).sort_values('mz_obs', ascending=False)
else:
    df = y.found.sort_values('mz_obs', ascending=False)
    
doc = docx_table_unique(df, parent, color_rgb, doc,
                        db = db,
                        write_termini=False)

doc.add_paragraph('')

if hasattr(y, 'found_ambig'):
    df = y.found_ambig.sort_values('mz_obs', ascending=False)
    #df.frags = y.found_ambig.frags.apply(lambda x: [z for z in np.unique(x)]).apply(lambda x: [[x]])
    doc = docx_table_ambig(df, parent, color_rgb, doc,
                           db = db,
                           write_termini=False)

section = doc.sections[-1]
section.orientation = WD_ORIENT.LANDSCAPE
new_width, new_height = section.page_height, section.page_width
section.page_width = new_width
section.page_height = new_height

doc.save('%s_underline.docx' % basename)
