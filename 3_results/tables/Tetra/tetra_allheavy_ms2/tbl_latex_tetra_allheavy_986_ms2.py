#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  3 15:00:46 2021

@author: kantundpeterpan
"""
import pylatex as pl
from pylatex import Document, Tabular, LongTable
from pylatex import TextColor as TC
import sys
import re
import os
sys.path.append('/media/kantundpeterpan/fast2/confinement/Python')
from MassAnalyzer.MS2Helper.FragDB.MonomersMS2DB import MonomersMS2DB
from MassAnalyzer.MS2Helper.FragDB.MS2DB import DimersMS2DB
from MassAnalyzer.MS2Helper.Tables import *
import pandas as pd
import numpy as np


color_map = {
        'C13N15':'heavy',
        'C12N14':'light'
    }

analysis_file = '/media/kantundpeterpan/fast2/confinement/Project Peptidoglycan/Exp 3 - M1 deriv M9 minimal heavy to light/analysis_scripts/Tetra_all_heavy_986.518_ms2.py'.replace(' ', '\\ ')

import IPython
ipython = IPython.get_ipython()
ipython.run_line_magic('run', '-i %s True' % analysis_file)

basename = 'tbl_tetra_986.518_ms2'
parent = 'Tetra'
parent_isotops =  ['all heavy']
parent_prec = 986.5158415198
db = MonomersMS2DB

colors = [('heavy', 'HTML', 'FF3333'), ('light', 'HTML', '9933FF')]

doc = Document()

doc.packages.append(pl.Package('xcolor'))
doc.packages.append(pl.Package('lscape'))
for c in colors:
    doc.add_color(*c)
    
if not os.path.exists(os.getcwd()+'/latex'):
    os.mkdir(os.getcwd()+'/latex')
    
if hasattr(y, 'found_unique'):
    df = pd.concat(y.found_unique).sort_values('mz_obs', ascending=False)
else:
    df = y.found.sort_values('mz_obs', ascending=False)
    
parent_table = latex_table_parent(parent, parent_isotops, parent_prec,
                                  color_map, db)
parent_table.dump(open('latex/%s_parent.tex' % basename, 'w'))

table_one = latex_table_unique(df, parent, color_map, db,
                               write_termini=False)

table_one.dump(open('latex/%s_one.tex' % basename, 'w'))

table_two = None
if hasattr(y, 'found_ambig'):
    df = y.found_ambig.sort_values('mz_obs', ascending=False)
    df.frags = y.found_ambig.frags.apply(lambda x: [z for z in np.unique(x)]).apply(lambda x: [[x]])

    table_two = latex_table_ambig(df, parent, color_map, db,
                                   write_termini=False)

doc.append(pl.NoEscape('\\begin{landscape}'))
doc.append(parent_table)
doc.append(table_one)
#doc.append(pl.NoEscape('\\small'))
if table_two:
    doc.append(table_two)
    table_one.dump(open('latex/%s_two.tex' % basename, 'w'))
    
doc.append(pl.NoEscape('\\end{landscape}'))

doc.generate_pdf(basename)

