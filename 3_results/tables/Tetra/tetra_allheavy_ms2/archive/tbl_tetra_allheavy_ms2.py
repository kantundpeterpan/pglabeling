#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 29 18:24:42 2021

@author: kantundpeterpan
"""
import sys
import pandas as pd
sys.path.append('/media/kantundpeterpan/fast2/confinement/Python')
from MassAnalyzer.MS2Helper.FragDB.MonomersMS2DB import MonomersMS2DB
pd.set_option('display.max_colwidth',None)
pd.options.display.float_format = "{:,.3f}".format

color_map = {
        'C13N15':'heavy',
        'C12N14':'light'
    }

sp = MonomersMS2DB['Tetra']['all heavy']

columns = ['Fragment']

#apply function to frag columns
test = y.found.frags[11][0]

def frag_string(frag, xspecies):
    slist = []
    for r in frag:
        lab = xspecies.mol.nodes[r]['Residue'].labeling.to_string()
        color = color_map[lab]
        r_string = r'{\textcolor{%s}{%s}}' % (color, r)
        slist.append(r_string.replace('_', ''))
        
    return slist
    
first = y.found.frags.apply(lambda x:frag_string(x[0],  sp))
second = first.apply(lambda x:'-'.join(x))

df_cols = ['mz_obs', 'mz_calc', 'ppm', 'y']

table = y.found.set_index(second)[df_cols].sort_values('mz_obs', ascending=False)
table.index.name = 'Fragment'
table.columns = ['$m/z_{obs}$', '$m/z_{calc}$', '$ppm$', '$Intensity (a.u.)$']

latex = open('../template.tex').read()

with open('ms2_table.tex', 'w') as f:
    f.write(latex.replace('${text_here}$', table.to_latex(escape=False, longtable=False).replace(r'\toprule','\\hline').replace('\\midrule', '\\hline').replace('\\bottomrule', '\\hline')))
