#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 29 18:24:42 2021

@author: kantundpeterpan
"""
import sys
import pandas as pd
import re
from subprocess import call
sys.path.append('/media/kantundpeterpan/fast2/confinement/Python')
from MassAnalyzer.MS2Helper.FragDB.MonomersMS2DB import MonomersMS2DB
from MassAnalyzer.MS2Helper.Tables import docx_table_unique
pd.set_option('display.max_colwidth',None)
pd.options.display.float_format = "{:,.3f}".format

color_map = {
        'C13N15':'heavy',
        'C12N14':'light'
    }

color_rgb = {
        'C13N15':'ff3333',
        'C12N14':'9933ff'
    }

basename = 'tbl_tetra_h2_ms2'
parent = 'Tetra'
df = y.found.sort_values('mz_obs', ascending=False)
docx_table_unique(df, 'test.docx', 'Tetra', color_map = color_rgb)

def frag_string(frag, xspecies):
    slist = []
    for r in frag:
        lab = xspecies.mol.nodes[r]['Residue'].labeling.to_string()
        color = color_map[lab]
        r_string = r'{\textcolor{%s}{%s}}' % (color, r)
        r_string = re.sub('\d+!', '', r_string)
        slist.append(r_string.replace('_', ''))
        
    return slist
    
first = y.found.frags.apply(lambda x:frag_string(x[0],  sp))

second = first.apply(lambda x:'-'.join(x))
second = second.apply(lambda x:x.replace('GlcNRed', 'GlcN\\textsuperscript{Red}'))
df_cols = ['mz_obs', 'mz_calc', 'ppm', 'y']

table = y.found.set_index(second)[df_cols].sort_values('mz_obs', ascending=False)
table.index.name = 'Fragment'
table.y = table.y.astype(int)
table.columns = ['$m/z_{obs}$', '$m/z_{calc}$', '$ppm$', '$Intensity (a.u.)$']

rawlatex = table.to_latex(escape=False, longtable=False).replace(r'\toprule','\\hline').replace('\\midrule', '\\hline').replace('\\bottomrule', '\\hline')
with open('%s.tex' % basename, 'w') as f:
    f.write(rawlatex)

latex = open('../template.tex').read()

with open('ms2_table_%s.tex' % basename, 'w') as f:
    f.write(latex.replace('${text_here}$', table.to_latex(escape=False, longtable=False).replace(r'\toprule','\\hline').replace('\\midrule', '\\hline').replace('\\bottomrule', '\\hline')))

call('pdflatex ms2_table_%s.tex' % basename, shell=True)
