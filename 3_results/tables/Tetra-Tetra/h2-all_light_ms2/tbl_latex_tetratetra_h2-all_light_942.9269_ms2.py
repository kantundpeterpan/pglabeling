#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  3 15:00:46 2021

@author: kantundpeterpan
"""
import pylatex as pl
from pylatex import Document, Tabular, LongTable
from pylatex import TextColor as TC
import sys
import re
sys.path.append('/media/kantundpeterpan/fast2/confinement/Python')
from MassAnalyzer.MS2Helper.FragDB.MonomersMS2DB import MonomersMS2DB
from MassAnalyzer.MS2Helper.FragDB.MS2DB import DimersMS2DB
from MassAnalyzer.MS2Helper.Tables.LaTeX_underline_recover import *
import pandas as pd

color_map = {
        'C13N15':'heavy',
        'C12N14':'light'
    }

analysis_file = '/media/kantundpeterpan/fast2/confinement/Project Peptidoglycan/Exp 13 - BW25113 d6ldts M9 hy to lg/analysis_scripts/exp13_TetraTetra_942.9_ms2.py'.replace(' ', '\\ ')

import IPython
ipython = IPython.get_ipython()
ipython.run_line_magic('run', '-i %s True' % analysis_file)

basename = 'tbl_tetratetra_h2-all_light_942.9269_ms2_xp13'
parent = 'Tetra-Tetra'
db = DimersMS2DB

colors = [('heavy', 'HTML', 'FF3333'), ('light', 'HTML', '9933FF')]

doc = Document()

doc.packages.append(pl.Package('xcolor'))
doc.packages.append(pl.Package('lscape'))
for c in colors:
    doc.add_color(*c)

table_one = latex_table_unique(pd.concat(y.found_unique), 'Tetra-Tetra', color_map, db,
                               write_termini=False)

table_two = latex_table_ambig(y.found_ambig, 'Tetra-Tetra', color_map, db,
                               write_termini=False)

doc.append(pl.NoEscape('\\begin{landscape}'))

doc.append(table_one)
#doc.append(pl.NoEscape('\\small'))
doc.append(table_two)

doc.append(pl.NoEscape('\\end{landscape}'))

doc.generate_pdf(basename+'_underlined')
