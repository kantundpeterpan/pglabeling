#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 29 18:24:42 2021

@author: kantundpeterpan
"""
import sys
import pandas as pd
import re
from subprocess import call
sys.path.append('/media/kantundpeterpan/fast2/confinement/Python')

from MassAnalyzer.MS2Helper.FragDB.MonomersMS2DB import MonomersMS2DB
from MassAnalyzer.MS2Helper.Tables import *
from docx import Document
from docx.enum.section import WD_ORIENT

analysis_file = '/media/kantundpeterpan/fast2/confinement/Project Peptidoglycan/Exp 3 - M1 deriv M9 minimal heavy to light/analysis_scripts/Tetra_h2_961.41_ms2.py'.replace(' ', '\\ ')

import IPython
ipython = IPython.get_ipython()
ipython.run_line_magic('run', '-i %s False' % analysis_file)

pd.set_option('display.max_colwidth',None)
pd.options.display.float_format = "{:,.3f}".format

color_map = {
        'C13N15':'heavy',
        'C12N14':'light'
    }

color_rgb = {
        'C13N15':'ff3333',
        'C12N14':'9933ff'
    }

basename = 'tbl_tetra_h2_961.41'
parent = 'Tetra'
parent_isotops =  ['h2']
parent_prec = 961.451412195386
db = MonomersMS2DB

doc = Document()

doc = docx_table_parent(parent, parent_isotops, parent_prec,
                        color_rgb, doc, db)

doc.add_paragraph('')

if hasattr(y, 'found_unique'):
    df = pd.concat(y.found_unique).sort_values('mz_obs', ascending=False)
else:
    df = y.found.sort_values('mz_obs', ascending=False)
    
doc = docx_table_unique(df, parent, color_rgb, doc,
                        db = db,
                        write_termini=False)

doc.add_paragraph('')

if hasattr(y, 'found_ambig'):
    df = y.found_ambig.sort_values('mz_obs', ascending=False)
    df.frags = y.found_ambig.frags.apply(lambda x: [z for z in np.unique(x)]).apply(lambda x: [[x]])
    doc = docx_table_ambig(df, parent, color_rgb, doc,
                           db = db,
                           write_termini=False)

section = doc.sections[-1]
section.orientation = WD_ORIENT.LANDSCAPE
new_width, new_height = section.page_height, section.page_width
section.page_width = new_width
section.page_height = new_height

doc.save('%s.docx' % basename)