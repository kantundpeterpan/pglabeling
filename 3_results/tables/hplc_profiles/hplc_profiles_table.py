#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 22 17:39:29 2021

@author: kantundpeterpan
"""
import pandas as pd
pd.options.display.float_format = "{:,.4f}".format
df = pd.read_excel('/media/kantundpeterpan/fast2/confinement/Project Peptidoglycan/HPLC_master.xlsx',
                   engine='openpyxl')

x = df.groupby(['experiment', 'sample'])
area_col = 'area_corr'
y = x.apply(lambda g: g[area_col]/g[area_col].sum())
z = pd.merge(df, y.reset_index(), right_on='level_2', left_index=True)

# mod for stems
'''
cols = z.columns.to_list()
cols[-1] = 'area_corr_y'
z.columns = cols
'''

d6 = z.set_index('experiment_x').loc[['xp13', 'xp20']]
d6_azt = z.set_index('experiment_x').loc[['xp14A']]
d6_mec = z.set_index('experiment_x').loc[['xp14B']]
m15 = z.set_index('experiment_x').loc[['xp3', 'xp18']]
m15_amp = z.set_index('experiment_x').loc[['xp17', 'xp23']]

b = d6.groupby('murop')
d6_mean = b.area_corr_y.mean().loc[['Tetra', 'Tetra-Tetra']].round(4)*100
d6_std = b.area_corr_y.std().loc[['Tetra', 'Tetra-Tetra']].round(4)*100

b_azt = d6_azt.groupby('murop')
d6_azt_mean = b_azt.area_corr_y.mean().loc[['Tetra', 'Tetra-Tetra']].round(4)*100
d6_azt_std = b_azt.area_corr_y.std().loc[['Tetra', 'Tetra-Tetra']].round(4)*100

b_mec = d6_mec.groupby('murop')
d6_mec_mean = b_mec.area_corr_y.mean().loc[['Tetra', 'Tetra-Tetra']].round(4)*100
d6_mec_std = b_mec.area_corr_y.std().loc[['Tetra', 'Tetra-Tetra']].round(4)*100

a = m15.groupby('murop')
m15_mean = a.area_corr_y.mean().loc[['Tri', 'Tetra', 'Tri-Tri', 'Tetra-Tri', 'Tri-Tetra', 'Tetra-Tetra']].round(4)*100
m15_std = a.area_corr_y.std().loc[['Tri', 'Tetra', 'Tri-Tri', 'Tetra-Tri', 'Tri-Tetra', 'Tetra-Tetra']].round(4)*100

c = m15_amp.groupby('murop')
m15_amp_mean = c.area_corr_y.mean().loc[['Tri', 'Tetra', 'Penta', 'Tri-Tri', 'Tetra-Tri', 'Tri-Tetra', 'Tri-Penta']].round(4)*100
m15_amp_std = c.area_corr_y.std().loc[['Tri', 'Tetra', 'Penta', 'Tri-Tri', 'Tetra-Tri', 'Tri-Tetra', 'Tri-Penta']].round(4)*100

ordered_murop = ['Tri', 'Tetra', 'Penta', 'Tri-Tri', 'Tri-Tetra', 'Tri-Penta', 'Tetra-Tri', 'Tetra-Tetra']
header_line = ' & '.join(['Strain', '$\\beta$-lactam', 'N\\textsuperscript{a}', 'Tri', 'Tetra', 'Penta',
                          'Tri$\\rightarrow$Tri', 'Tri$\\rightarrow$Tetra', 'Tri$\\rightarrow$Penta',
                          'Tetra$\\rightarrow$Tri', 'Tetra$\\rightarrow$Tetra']) + '\\\\\n'

table_lines = []
table_lines.append('\\begin{tabular}{llc|cccccccc}\n')
table_lines.append(' & & & \\multicolumn{8}{c}{\\textbf{Relative abundance (mean $\\pm$ standard deviation)}}\\\\\n')
table_lines.append(header_line)
table_lines.append('\\hline\n')

perc_style = '%.0f $\pm$ %.0f'

d6_none_line = ' & '.join(['BW25113 $\\Delta$6\\emph{ldt}', 'None','%i' % (d6.shape[0]/2),
                           ] + [perc_style % (d6_mean[mp], d6_std[mp]) if mp in d6_mean.index else 'ND' for mp in ordered_murop]) + '\\\\\n'
table_lines.append(d6_none_line)

d6_azt_line = ' & '.join(['', 'AZT 12', '%i' % (d6_azt.shape[0]/2)
                         ] + [perc_style % (d6_azt_mean[mp], d6_azt_std[mp]) if mp in d6_azt_mean.index else 'ND' for mp in ordered_murop]) + '\\\\\n'
table_lines.append(d6_azt_line)

d6_mec_line = ' & '.join(['', 'MEL 2.5','%i' % (d6_mec.shape[0]/2),
                         ] + [perc_style % (d6_mec_mean[mp], d6_mec_std[mp]) if mp in d6_mec_mean.index else 'ND' for mp in ordered_murop]) + '\\\\\n'
table_lines.append(d6_mec_line)

table_lines.append('\\hline\n')

m15_none_line = ' & '.join(['M1.5', 'None', '%i' % (m15.shape[0]/6),
                           ] + [perc_style % (m15_mean[mp], m15_std[mp]) if mp in m15_mean.index else 'ND' for mp in ordered_murop]) + '\\\\\n'
table_lines.append(m15_none_line)

m15_amp_line = ' & '.join(['', 'AMP 16', '%i' % (m15_amp.shape[0]/6),
                          ] + [perc_style % (m15_amp_mean[mp], m15_amp_std[mp]) if mp in m15_amp_mean.index else 'ND' for mp in ordered_murop]) + '\\\\\n'
table_lines.append(m15_amp_line)

table_lines.append('\\end{tabular}')

with open('muroptable.tex', 'w') as f:
    for line in table_lines:
        f.write(line)
