#!/usr/bin/env python
# coding: utf-8

# In[4]:


import matplotlib.pyplot as plt
import matplotlib
import pandas as pd
import os

matplotlib.rc('font', **{'family':'serif', 'serif':['Asana']})
matplotlib.rcParams['text.usetex'] = True
from glob import glob


files = sorted(glob('./data_fig2/*.csv'))
dfs = [pd.read_csv(f) for f in files]
dfs[1].rel_intensity.max()

fig,axs = plt.subplots(ncols=3, nrows=2, figsize=(15,8), facecolor='white')

axs = axs.ravel()

heavy = axs[:3]
light = axs[3:]

#######################
### Font Parameters ###
#######################

linewidth=1
fontsize_x = 15
fontsize_y = 15
fontsize_ax_title = 18
labelsize_xy = 14

######################

xlabel = r'$m/z$'
ylabel = r'rel. Intensity'

for ax,df,c in zip(pd.np.hstack([axs[:2],axs[3:-1]]), dfs, ('C0', 'C1')*2):
    ax.plot(df.mz, df.rel_intensity, c=c, linewidth=linewidth)

heavy[-1].plot(dfs[0].mz, dfs[0].rel_intensity, linewidth=linewidth)
heavy[-1].plot(dfs[1].mz, dfs[1].rel_intensity, '--', linewidth=linewidth)

for ax in heavy:
    ax.set_xlim(908, 914)
    ax.set_xlabel(xlabel, fontsize = fontsize_x)
    ax.set_ylabel(ylabel, fontsize = fontsize_y)
    ax.tick_params(labelsize=labelsize_xy)

light[-1].plot(dfs[2].mz, dfs[2].rel_intensity, linewidth=linewidth)
light[-1].plot(dfs[3].mz, dfs[3].rel_intensity, '--', linewidth=linewidth)

for ax in light:
    ax.set_xlim(871, 876)
    ax.set_xlabel(xlabel, fontsize = fontsize_x)
    ax.set_ylabel(ylabel, fontsize = fontsize_y)
    ax.tick_params(labelsize=labelsize_xy)

#ax_titles = ['A - GM-Tripeptide uniformly labeled, experiment', 'B - GM-Tripeptide uniformly labeled, simulation', 'C - Overlay',\
#             'A - GM-Tripeptide uniformly labeled, experiment', 'B - GM-Tripeptide uniformly labeled, simulation','C - Overlay'\
#]

ax_titles = [r'\textbf{%s}' % (t) for t in list('ABCDEF')]

for a,t in zip(axs, ax_titles):
    a.set_title(t, ha='left', x=0, fontsize = fontsize_ax_title)

fig.tight_layout()

plt.subplots_adjust(wspace=0.2, hspace=0.2)
fig.canvas.draw()

fig.savefig('Figure2.png', dpi=300)


# In[ ]:




