#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 20 17:22:26 2020

@author: kantundpeterpan
"""

import pickle as pkl
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

dfs = pkl.load(open('df_list_tri.pkl','rb'))

dx = 15
dy = 0.25

x = 0
ys = []

fig, ax = plt.subplots(figsize=(8,6))

for i,df in enumerate(dfs):
    #ys.append(y)
    ax.plot(df.x-i*dx,
             (df.y/df.y.max())*1+i*dy,
             zorder = -i, color='k', linewidth=5,
             alpha=0.7)
    ax.plot(df.x-i*dx,
             (df.y/df.y.max())*1+i*dy,
             zorder = -i, linewidth=1, alpha = 0.7)

all_light = 871.5
all_heavy = 911.5

xline = np.linspace(all_light-4*dx-4,all_light+4)
slope, intcp = np.polyfit([all_light, all_light-dx], [0, dy],1)
yline = slope*xline+intcp

l = ax.plot(xline, yline, linestyle='--', zorder=-200, color='k', linewidth=1)



xline = np.linspace(all_heavy-4*dx-4,all_heavy+4)
slope, intcp = np.polyfit([all_heavy, all_heavy-dx], [0, dy],1)
yline = slope*xline+intcp

l.append(ax.plot(xline, yline, linestyle='--', zorder=-200, color='k', linewidth=1)[0])
