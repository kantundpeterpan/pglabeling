% Created 2021-07-12 Mon 16:26
% Intended LaTeX compiler: pdflatex
\documentclass[11pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\linespread{1.5}
\usepackage[margin=1in,footskip=0.25in]{geometry}
\usepackage{setspace}
\usepackage[font=small,labelfont=bf,tableposition=bottom]{caption}
\captionsetup[figure]{font={stretch=1.1}}
\DeclareCaptionLabelFormat{supp_cap}{#1 S#2}
\captionsetup[suppfigure]{font={stretch=1.1}, labelformat=supp_cap}
\captionsetup[supptable]{font={stretch=1.1}, labelformat=supp_cap}
\newcounter{suppdata_global}
\DeclareCaptionLabelFormat{suppdata}{#1 \arabic{suppdata_global}.#2}
\captionsetup[suppdata]{font={stretch=1.1}, labelformat=suppdata}
\usepackage{wrapfig}
\setcounter{secnumdepth}{5}
\usepackage{breakcites}
\usepackage{subcaption}
\usepackage{adjustbox}
\usepackage{pdfpages}
\usepackage[shortlabels]{enumitem}
\usepackage{tikz}
\usetikzlibrary{trees}
\usepackage{forest}
\input{/home/kantundpeterpan/my_stuff_on_server/thesis/master/forest_setup.tex}
\usepackage{newfloat}
\DeclareFloatingEnvironment[name={Supplementary Figure}, listname={List of Supplementary Figures}]{suppfigure}
\DeclareFloatingEnvironment[name={Supplementary Table}, listname={List of Supplementary Figures}]{supptable}
\DeclareFloatingEnvironment[name={Supplementary Data}, listname={}]{suppdata}
\usepackage{booktabs}
\usepackage{multirow}
\setcounter{secnumdepth}{6}
\author{haina}
\date{\today}
\title{}
\hypersetup{
 pdfauthor={haina},
 pdftitle={},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.3 (Org mode 9.4.4)}, 
 pdflang={English}}
\begin{document}

\graphicspath{{./figures/}}
\section{Discussion}
\label{7:pg_label:discussion}
\paragraph*{Peptidoglycan labeling schemes.}
\label{sec:org4daee6f}
Initial studies of the expansion of the peptidoglycan macromolecule relied on labeling with [\textsuperscript{3}H] or [\textsuperscript{14}C]DAP \cite{DeJonge1989}. Complementary imaging was based on the incorporation of D-cysteine at the fourth position of tetrapeptide stems by an enzymatic activity that was unknown at that time \cite{DePedro1997} and that most likely corresponds to the exchange activity of \textsc{l,d}-transpeptidases \cite{Mainardi2005}.
Free thiol groups of cysteine residues were biotinylated, enabling immunodetection by transmission electron microscopy. These approaches revealed single-strand and diffuse insertion of newly synthesized glycan chains into the lateral cell wall whereas synthesis of the septum relied on insertion of multiple strands at a time in sharply outlined growth zones \cite{DePedro1997}. Quantitatively, the number of glycan strands inserted at a time was previously estimated by determining the acceptor-donor radioactivity ratio (ADRR), \emph{i.e}. the radioactivity ratio in the acceptor and donor positions of dimers (Supplementary Fig. S\ref{pg_label:intro:sfig:adrr_calcs}) \cite{Burman1984a}.
By analogy, we define here the acceptor-donor ratio in neo-synthesized stems (ADRNS), which is more accurate since it takes into account hybrid isotopologues containing unlabeled and labeled moieties (see below). ADRR determination was based on pulse labeling with radioactive DAP whereas ADRNS was based on determination of the incorporation of neo-synthesized subunits into the fully labeled pre-existing peptidoglycan. The latter strategy was chosen because of the ease of obtaining fully labeled cells with non-radioactive heavy isotopes thereby providing direct access to the absolute amount of isotopologues.
Switching the medium from labelled to unlabelled was chosen since it facilitates the detection of neo-synthesized isotopologues present at a low abundance at early times after the medium switch. Indeed, the corresponding isotopic clusters do not appear in the region of the mass spectra containing the sodium adducts of the fully labeled isotopologues, which outnumber pre-existing isotopologues at the beginning of the kinetics. Preliminary experiments (data not shown) showed that this is paramount since the opposite labeling scheme involving an unlabeled-to-labelled medium switch resulted in the presence of neo-synthesized and pre-existing isotopologues in partially overlapping isotopic clusters.

\paragraph*{Mode of insertion of subunits into the growing peptidoglycan network by PBPs.}
\label{sec:org9fd2a8b}
Since the mode of insertion of new peptidoglycan subunits into the growing peptidoglycan varies during the cell cycle various approaches were developed for specifically investigating expansion of the lateral cell wall and the synthesis of the septum.
These approaches relied on the use of synchronized cells \cite{DeJonge1989}, specific inhibitors of proteins of the elongasome and of the divisome \cite{Botta1981,AngelDePedro1981}, thermosensitive mutants deficient in cell elongation or cell division at the non-permissive temperature \cite{Burman1984a}, and overproduction of the cell division inhibitor SulA \cite{Uehara2008,Cho2016}.
In our study, we used aztreonam and mecillinam to specifically inhibit synthesis of septal and lateral peptidoglycan, respectively, in strain BW25113 \(\Delta 6ldt\), which exclusively relies on PBPs for cross-linking. In the presence of aztreonam, the acceptor-donor ratio of unlabeled (neo-synthesized) material (ADRNS) was 0.14 five minutes after the medium switch and increased to 0.20, 0.34, 0.50, and 0.64 at 20, 40, 60, and 80 minutes, respectively (Supplementary Fig. S\ref{pg_label:discussion:sfig:adrns}). At this time scale, the pre-existing (labeled) disaccharide-peptides are gradually replaced by neo-synthesized (unlabeled) disaccharide-peptides accounting for the fact that neo-synthesized subunits are increasingly cross-linked to unlabeled disaccharide-peptide subunits generating dimers with an ADRNS of 1 (unlabeled donor and acceptor). The extrapolation of the ADRNS at t=0 (time of the medium switch) is therefore relevant to the evaluation of the mode of insertion of new subunits into the peptidoglycan. The extrapolated ADRNS value of 0.08 (Supplementary Fig. S\ref{pg_label:discussion:sfig:adrns}) was similar to the ADRR value of 0.10 reported in reference \cite{DeJonge1989}, in that case obtained by extrapolating data to a synchronized culture exclusively containing elongating cells.
In the presence of mecillinam, the ADRNS values marginally increased over time, providing an extrapolated value of 0.31 for t=0. This value is lower than the ADRR of 0.64 reported by De Jonge et al. (1989) \cite{DeJonge1989} by extrapolating data to a synchronized culture exclusively containing constricting cells. These values correspond to the insertion of approximately two or five strands at a time (calculated ADR of 0.33 and 0.67, respectively, see Introduction and Supplementary Figure S\ref{pg_label:intro:sfig:adrr_calcs}). The origin of this difference is unknown but may involve multiple causes including the fact that blocking cell elongation by mecillinam may not fully prevent incorporation of newly synthesized subunits because mecillinam does not block the polymerization of glycan chains by the elongasome \cite{Uehara2008}.
Together, our results are in agreement with the analysis reported by DeJonge \emph{et al}. (1989) \cite{DeJonge1989} showing single-strand insertion of neo-synthesized peptidoglycan in the side wall \emph{versus} insertion of multiple strands for septal peptidoglycan synthesis. In the absence of mecillinam and aztreonam, the ADRNS value (0.14) was intermediate between the values observed in the presence of aztreonam (0.08) and mecillinam (0.31), as qualitatively expected for the combined contributions of cross-links located in the side wall and in the septum and the lower contribution of the latter to the cell surface.

\begin{suppfigure}
\centering
\includegraphics[width=0.75\linewidth]{adrns/adrns_d6.png}
\caption[Acceptor to donor ratio of neo-synthesized stems (ADRNS) of \emph{E. coli} $\Delta 6ldt$]{\textbf{Acceptor to donor ratio of neo-synthesized stems (ADRNS) of \emph{E. coli} $\Delta 6ldt$.}
}
\label{pg_label:discussion:sfig:adrns}
\end{suppfigure}

\paragraph*{Mode of insertion of subunits involving the combined cross-linking activity of PBPs and YcbB.}
\label{sec:orgc1a1ca6}
In the absence of \(\beta\)-lactam, \emph{E. coli} M1.5 \cite{Hugonnet2016,Voedts2021} relies both on the \textsc{d,d}-transpeptidase activity of PBPs and the \textsc{l,d}-transpeptidase activity of YcbB for peptidoglycan cross-linking, leading to high proportions of 4→3 (56\%) and 3→3 (44\%) cross-linked dimers (Table S\ref{pg_label:results:stbl:murop_comp}).
The proportions of dimers containing two neo-synthesized stems (new→new) or one neo-synthesized and one pre-existing stem (new→old) were similar for the two types of cross-links (Fig. \ref{pg_label:results:fig:dimers_tc}).
Thus, the \textsc{d,d}-transpeptidase activity of PBPs and the \textsc{l,d}-transpeptidase activity of YcbB were involved in similar modes of insertion of new strands into the preexisting peptidoglycan of strain M1.5. The ADRNS values were in the order of 0.17 and 0.22 for 4\(\rightarrow\)3 and 3\(\rightarrow\)3 crosslinks, respectively.
These values indicate that the neo-synthesized glycan strands are predominantly but not exclusively cross-linked to the preexisting peptidoglycan for both types of transpeptidation. Peptidoglycan polymerization might involve single-strand insertion in the side-wall and insertion of multiple strands in the septum as discussed above for strain \(\Delta 6ldt\). These observations suggest that the mode of insertion of new subunits is not determined by the transpeptidases but by other components of the peptidoglycan polymerization complexes, such as the scaffolding proteins.

\paragraph*{Mode of insertion of subunits involving exclusive cross-linking by the \textsc{l,d}-transpeptidase activity of YcbB.}
\label{sec:org54f42fb}
Growth of \emph{E. coli} M1.5 in the presence of 16 µg/mL ampicillin resulted in a peptidoglycan almost exclusively cross-linked by YcbB due to the inhibition of the \textsc{d,d}-transpeptidase activity of PBPs (Fig. \ref{pg_label:results:fig:m15amp_tc}).
Strikingly, the Tri→Tetra and Tri→Penta dimers were almost exclusively of the new→new type. This mode of insertion was not found in strain M1.5 grown in the absence of ampicillin (Fig. \ref{pg_label:results:fig:dimers_tc}) and in strain \(\Delta 6ldt\) grown in the presence of aztreonam or in the absence of antibiotic (Fig. \ref{pg_label:results:fig:d6_tc}).
The large proportion of new→new dimers may originate from the inhibition of the transpeptidase activity of PBPs by ampicillin since inactivation of PBPs by \(\beta\)-lactams is known to result in the uncoupling of the cross-linking and transglycosylase reactions \cite{Uehara2008,Cho2014}.
This in turn results in the accumulation of linear glycan strands that might be cross-linked to each other by YcbB \cite{Voedts2021}. Accumulation of linear glycan strands potentially at the origin of the synthesis of the new→new dimers formed by YcbB is also expected to be stimulated by a mutation present in M1.5 that impairs the activity of lytic transglycosylase SltY and prevents the degradation of uncross-linked glycan strands \cite{Voedts2021}. The new→new pattern of insertion almost exclusively detected in the Tri→Tetra and Tri→Penta dimers (Fig. \ref{pg_label:results:fig:m15amp_tc}) corresponds to the insertion of multiple strands at a time.
Thus, the neo-synthesized material was only connected to the preexisting peptidoglycan by the formation of Tri→Tri dimers, which are the only new→old dimers detected in the peptidoglycan. This implies that the acceptors of the cross-linking reactions in the preexisting peptidoglycan were almost exclusively tripeptides, whereas preexisting etrapeptides also acted as acceptors in the absence of ampicillin (Fig. \ref{pg_label:results:fig:d6_tc}).
The exclusive use of pre-existing tripeptides as acceptors in the presence of ampicillin could be accounted for by the fact that these tripeptides originate from cleavage of 3→3 cross-links liberating the tripeptide stems present in the donor position of the preexisting dimers. This model implies a cooperation between YcbB, which catalyzes the formation of the Tri→Tri cross-links, and an endopeptidase activity, which provides the tripeptide acceptor substrate of this reaction by cleaving preexisting 3→3 cross-links. This endopeptidase activity could correspond to that of MepK since this endopeptidase preferentially cleaves 3→3 cross-links \emph{in vitro}, is essential for bacterial growth only in strains overproducing YcbB, and is not inhibited by ampicillin \cite{Voedts2021}.
In the absence of ampicillin, the presence of 4→3 cross-links and of active endopeptidases belonging to the PBP family could contribute to the diversity of acceptors available for peptidoglycan cross-linking in mutant M1.5 leading to the formation of dimers containing both tripeptide and tetrapeptide stems in the acceptor position of dimers.

\paragraph*{Extent of peptidoglycan recycling.}
\label{sec:org114e5f3}
In the absence of turnover and recycling, the number of fully labeled disaccharide-peptide subunits is expected to remain constant. Turnover, corresponding to a small proportion of peptidoglycan degradation products \cite{Goodell1985}, results in a reduction in the content of fully labeled disaccharide-peptides through loss of peptidoglycan fragments in the external medium.
Recycling also results in a reduction in the peptidoglycan content of fully labeled disaccharide-peptides since the recycled moieties are incorporated into peptidoglycan precursors in combination with neo-synthesized moieties according to the pathways depicted in Supplementary Fig. S\ref{pg_label:intro:sfig:pg_metabolism}.
These moieties include two glucosamine residues, two acetyl groups, one D-lactoyl residue, the \textsc{l}-Ala-\textsc{d}-iGlu-DAP tripeptide or its constitutive amino acids, and two \textsc{d}-Ala residues. Disaccharide-peptide units containng recycled fragments are therefore mixtures of labeled and unlabeled moieties.
These neo-synthesized chimeric structures are easily distinguished from fully labeled disaccharide-peptide subunits originating from peptidoglycan polymerized before the medium switch. Thus, the decrease in the absolute number of fully labeled disaccharide-peptide subunits in one generation directly provides an estimate of the fraction of the preexisting peptidoglycan that is hydrolyzed during each generation. The decrease in the amount of fully labeled disaccharide-peptide subunits per generation was estimated to be 39\% for strain \(\Delta 6ldt\) and 56\% or 78\% for M1.5 grown in the absence or presence of ampicillin (Supplementary Figure S\ref{pg_label:discussion:sfig:recycling}).

\begin{suppfigure}
\centering
\includegraphics[width=0.9\linewidth]{recycling/decrease_fully_labeled_stems.png}
\caption[Loss of fully labeled disaccharide-peptide subunits from pepitdoglycan]{\textbf{Loss of fully labeled disaccharide-peptide subunits from pepitdoglycan.} The relative amounts of fully labeled peptidoglycan disaccharide-peptides (normalized to 1.0 at the medium switch) decreased over time, which was normalized per strain to its respective generation time $g$ (Supp. Table S\ref{pg_label:results:stbl:gen_time}). The decrease was linear for M1.5 grown in the presence or absence of ampicillin. The extent of the combined turnover and recycling was estimated by linear regression of the full time course of the experiment. For BW25113$\Delta6ldt$, a decrease in the content of fully labeled stems was only observed after 0.45 generation times potentially reflecting the assembly of peptidoglycan from labeled precursors pools. For this reason, regression analysis was performed for a portion of the kinetics (from 0.45 to 0.85 generation time; data points excluded from regression analysis are shown as grey triangles). The accuracy of the estimates of the combined peptidoglycan turnover and recycling provided by this analysis is limited by the absence of experimental evidence of the size of the putative preexisting precursors pools and data collection for a limited time (less than a generation).}
\label{pg_label:discussion:sfig:recycling}
\end{suppfigure}

These figures are rough estimates since they are collected over a fraction of a generation time (0.66 to 0.95). The extent of the release of fragments from the peptidoglycan macromolecule has been previously evaluated by several approaches based on determination of radiolabeled DAP. In mutants deficient in the production of the AmpG and Opp permeases, the peptidoglycan fragments are not recycled in the cytoplasm and released in the culture medium \cite{Jacobs1994}.
Using this approach, Jacobs \emph{et al.} reported that \emph{ca.} 44\% of the peptidoglycan is released from the peptidoglycan layer each generation by the \emph{E. coli} K-12 strain MC4100. This value is similar to that observed in our study for strain \(\Delta 6ldt\) (39\%). The extent of peptidoglycan degradation was higher in mutant M1.5, in particular in the presence of ampicillin (78\%). This might result from the uncoupling of the transglycosylation and transpeptidation reactions (above), which was reported to lead to a futile cycle in which accumultation of uncross-linked glycan strands is associated with their partial degradation by autolysins \cite{Cho2014}.

\paragraph*{Advantages and limitations of peptidoglycan analyses based on incorporation of radioactive DAP \emph{versus} uniform labeling with stable isotopes.}
\label{sec:org0aab83f}
Most studies investigating PG metabolism based on labeling with radioactive DAP relied at least partially on \emph{E. coli} strain W7, a derivative of K-12 strain MC4100,  which is deficient in DAP synthesis and conversion of DAP to \textsc{l}-lysine due to deletions of the \emph{lysA} and \emph{dapF} genes, respectively \cite{Burman1984a,Glauner1990,Goodell1985,Goodell1985a}.
This strain is auxotroph for DAP and \textsc{l}-lysine and is generally grown in the presence of 2 µg/ml or more of DAP and, when grown in glucose minimal medium, 50 µg/ml of \textsc{l}-lysine.
The intracellular DAP pool in \emph{E. coli} W7 is larger than in wild-type \emph{E. coli K-12} strains (an estimated intracellular concentration of 31.5 \(mmol/l\) \cite{Wientjes1985} \emph{vs.} 0.6 \(mmol/l\) in \emph{E. coli} K-12 \cite{Mengin-Lecreulx1982}).
For \emph{E. coli} W7, the intracellular DAP pool exceeds the amount of DAP present in the peptidoglycan by a factor of five \cite{Wientjes1985}. As discussed by \emph{Glauner} and \emph{Höltje} (1990), this limits the incorporation of radioactive DAP into the sacculi and induces a lag between the addition of DAP and its incorporation \cite{Glauner1990}.
In order to improve the kinetics of incorporation of radiolabeled DAP, Burman \emph{et al.} (1984) depleted the intracellular DAP pool by transferring cultures to DAP-free medium for about 25 minutes prior to the addition of radiolabeled DAP \cite{Burman1984a}. However, electron microscopy imaging of sacculi showed that this treatment compromises the integrity and stability of the PG layer before the onset of lysis \cite{Burman1983}.
The perturbations resulting from DAP starvation also include changes in cell morphology \cite{Verwer1980} and reduced turnover \cite{Burman1983b}. These perturbations were slowly and only partially reversed by the addition of DAP after the starvation period. Our procedure did not suffer from these limitations since uniform labeling of the PG with stable isotopes does not require use of \emph{dapF} and \emph{lysA} deficient mutants. In our conditions, the intracellular DAP pool did not limit the labeling efficacy since hybrid muropeptides containing a labeled DAP moiety were not detected after the medium switch.
This is expected, since the intracellular DAP pool in wild-type K-12 strains represents 10\% of the DAP content of PG and at least 50\% of intracellular DAP is converted into \textsc{l}-lysine \cite{Wientjes1985}.

Another limitation of labeling procedures based on radioactive DAP is their inability to discriminate between muropeptides originating from pre-existing peptidoglycan, from recycling of the \textsc{l}-Ala-\textsc{d}-iGlu-DAP tripeptides, or from the preexisting UDP-MurNAc-pentapeptide pool. In contrast, our procedure based on uniform labeling with stable isotopes easily discriminates between these three types of muropeptides since they correspond to different isotopologues: uniformly labeled muropeptides for the preexisting peptidoglycan, hybrid h2 for recycled tripeptides (Fig. \ref{pg_label:results:fig:mono_tc}), and hybrid h3 for muropeptides issued from the intracellular pool of UDP-MurNAc-pentapeptide (Fig. \ref{pg_label:results:fig:mono_tc}). The latter type of muropeptides is likely to contribute to the lag phase observed for the incorporation of radioactive DAP into peptidoglycan. Accordingly, a lag phase was observed in our study when hybrids containing an [\textsuperscript{13}C]- and [\textsuperscript{15}N]- labeled DAP were excluded from the data set (Supplementary Fig. S\ref{pg_label:discussion:sfig:new_vs_hyb}).

The factors that limit the analysis of muropeptides at early times after the medium switch differ for the labeling techniques based on radioactive DAP and stable isotopes. Purification of sacculi using the hot SDS procedure is prone to sample loss due to numerous washing and digestions steps \cite{Desmarais2014}. According to the procedure implemented in our laboratory, the purification yield is severely impaired for extraction of peptidoglycan from culture volumes smaller than 200 mL. Methods based on DAP labeling offer the possibility to add unlabeled sacculi as carrier material \cite{Glauner1988} enabling experiments using small culture volumes (\emph{e.g.} 25 mL \cite{Glauner1990} \emph{versus} 200 mL in our study).
In turn, small culture volumes grant access to labeling times as short as 20 seconds \cite{Glauner1990} based on filtration of an unlabeled culture, \emph{in situ} labeling and immediate transfer of the filter to a boiling SDS solution.
Our methodology is limited in this regard, since it is not compatible with the addition of unlabeled carrier sacculi and thus requires substantially larger sample volumes that cannot be easily filtrated. This introduces a delay originating from the necessity to recover labeled bacteria by centrifugation.

\begin{suppfigure}
\centering
\includegraphics[width=0.45\linewidth]{newonly_vs_hybrids/Tet_radioDAP_vs_allhyb_hAla.png}
\caption[Comparing DAP labeling and \textsuperscript{13}C/\textsuperscript{15}N-labeling]{Kinetics of accumulation of
neo-synthesized disaccharide-tetrapeptide isotopologues containing
unlabeled DAP (pool 1, squares) \emph{versus} all neo-synthesized
disaccharide-tetrapeptide isotopologues (pool 2, solid circles). Pool 1
comprises the fully unlabeled muropeptide and hybrids h1 and hAla (see
Fig. 8 and Supplementary Data for the structure and identification of
these isotopologues). Pool 2 comprises all neo-synthesized isotopologues
including those containing an unlabeled DAP (Pool 1, fully unlabeled,
$h$1, and $h$Ala) plus those containing a labeled DAP ($h$2, $h$1$h$2, $h$3, and
$h$3Ala). Adding this set of muropeptides, which are not identified as
neo-synthesized by radioactive DAP labeling techniques, results in a
kinetics devoid of any lag phase.}
\label{pg_label:discussion:sfig:new_vs_hyb}
\end{suppfigure}

\section*{\ldots{}}
\label{sec:org8aaf1d7}
\clearpage
\bibliographystyle{apalike}
\bibliography{../../bibliography/library}
\end{document}
